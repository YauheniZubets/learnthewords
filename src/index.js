import React from 'react';
import ReactDom from 'react-dom';
import HeaderBlock from './components/HeaderBlock';
import './index.css';

const AppList = () => {
  const items=['Item 1', 'Item 2', 'Item 3'];
  const firstItem=<li>Item 0</li>;

  const isAuth=true;

  return (
  <ul>
    {isAuth ? firstItem : null}
    {items.map(item=><li>{item}</li>)}
    <li>{items[0]}</li>
    <li>{items[1]}</li>
  </ul>
  );
};

const AppHeader = () => {
  
  return <h1 className='header'>My header</h1>;
}

const AppInput=()=>{
  const placeholder='Type text...';

  return(
    <label>
      <input placeholder={placeholder}/>
    </label>
  )
}

const App = () => {
  return(
    <>
      <HeaderBlock></HeaderBlock>
      <AppInput/>
      <AppHeader/>
      <AppList/>
      <AppHeader/>
      <AppList/>
    </>
  )
}



ReactDom.render(<App/>, document.getElementById('root'));

